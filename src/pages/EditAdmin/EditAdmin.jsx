import '../AdminPage/AdminPage.css'
import React, { useState, useEffect } from "react";
import axios from "axios";

export default function EditAdmin(props) {

    const [state, setState] = useState({});
	let id=props.match.params.id;
	// console.log(id);

    useEffect(() => {
        async function fetchData(id) {
          await axios.get("//localhost:8000/api/getadminbyid/" + id)
          .then(res=>{
              let result=res.data;
              setState(result)
          })
            
            
            // console.log("hi",state);
        }
        fetchData(props.match.params.id);
      }, []);



      const handleChange = (e) => {
        // let { name, value } = e.target;
        // setState({
        //   ...state,
        //   [name]: value,
        // });
      };
    
      const handleEdit = async () => {
		  
        const url = "//localhost:8000/api/editadmin" + id;
        // let username = state.UserName;
        // let email = state.Email;
        // let number = state.Number;
        // let img = state.Image;
        // let password = state.Password;

		let username = "hilal";
        let email = "hh@gmail.com";
        let number = "55252";
        let img = "efe";
        let password = "ggrf";
        // let body = {};
    
        const formdata = new FormData();
        formdata.append(`UserName`, username);
        formdata.append(`Email`, email);
        formdata.append(`Number`, number);
        formdata.append(`Image`, img);
        formdata.append(`Password`, password);
        const response = await fetch(url, { method: "put",
		headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
			  },
		body:formdata });
        let result = await response.json();
        if (result.success) {
          alert("Save Success");
        } else {
          window.alert("Unable To Save");
        }
      };
	

	
	return (
		<div id="editEmployeeModal">
			<div class="modal-dialog">
				<div class="modal-content">
					<form onSubmit={handleEdit}>
						<div class="modal-header">
							<h4 class="modal-title">Edit Employee</h4>
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						</div>
						<div class="modal-body">
							<div class="form-group">


								<label>User Name</label>
								<input type="text"
									class="form-control"
									name="UserName"
									value={state.UserName}
									onChange={handleChange}
									required />
							</div>


							<div class="form-group">
								<label>Email</label>
								<input type="email"
									class="form-control"
									name="Email"
									value={state.Email}
									onChange={ handleChange}
									required />
							</div>


							<div class="form-group">
								<label>Password</label>
								<textarea class="form-control"
									name="Password"
									value={state.Password}
									onChange={ handleChange}
									required></textarea>
							</div>

							<div class="form-group">


								<label>Number</label>
								<input type="text"
									class="form-control"
									name="Number"
									value={state.Number}
									onChange={ handleChange}
									required />
							</div>

							<div class="form-group">


								<label>Image</label>
								<input id="fileupload" type="file" name="Image"
							 onChange={ handleChange}
                            //  value={state.Image}
							 />
							</div>


							{/* <div class="form-group">
								<label>Phone</label>
								<input type="text" class="form-control" required />
							</div> */}
						</div>
						<div class="modal-footer">
							<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel" />
							<input type="submit" class="btn btn-info" value="Save" />
						</div>
					</form>
				</div>
			</div>
		</div>
	);
}