import { Link } from "react-router-dom";
import "./AdminPage.css";
import AdminCard from "../../components/AdminCard/AdminCard";
import React, { useState, useEffect } from "react";

export default function AdminPage() {
  //const [error, setError] = useState({});

  const [admin, setAdmin] = useState([]);
  const [error, setError] = useState("");

  useEffect(() => {
    async function fetchData() {
      const res = await fetch("http://localhost:8000/api/getadmin");
      res
        .json()
        .then((res) => {
          //console.log(res[0]);
          //console.log(res);

          setAdmin(res);

          console.log(admin);
        })
        .catch((err) => console.log(err));
    }
    fetchData();

  }, []);


  const deleteAdmin = async (id) => {

    try {
      const response = await fetch(`//localhost:8000/api/deleteadmin/${id}`, {
        method: "delete",
      });

      console.log("hello", response);

    } catch (e) {
      setError(e)
    };

    let stateAdmins = [...admin].filter(ad => ad.id !== id);
    setAdmin(stateAdmins);
    alert("deleted successfuly")


  };
  return (
    <>
      <div class="container-xl adminpage">
        <div class="table-responsive">
          <div class="table-wrapper">
            <div class="table-title">
              <div class="row">
                <div class="col-sm-6">
                  <h2>
                    Manage <b>Admins</b>
                  </h2>
                </div>
                <div class="col-sm-6">
                  <Link
                    to="/addadmin"
                    class="btn btn-success"
                    data-toggle="modal"
                  >
                    <i class="material-icons">&#xE147;</i>{" "}
                    <span>Add New Admin</span>
                  </Link>
                  <a
                    href="#deleteEmployeeModal"
                    class="btn btn-danger"
                    data-toggle="modal"
                  >
                    <i class="material-icons">&#xE15C;</i> <span>Delete</span>
                  </a>
                </div>
              </div>
            </div>
            <table class="table table-striped table-hover">
              <thead>
                <tr>
                  <th>
                    <span class="custom-checkbox">
                      <input type="checkbox" id="selectAll" />
                      <label for="selectAll"></label>
                    </span>
                  </th>
                  <th>Image</th>
                  <th>User Name</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                {admin.map((ad) => (
                  <AdminCard
                    key={ad.id}
                    id={ad.id}
                    username={ad.UserName}
                    email={ad.Email}
                    image={ad.Image}
                    number={ad.Number}
                    delete={deleteAdmin}
                  />
                ))}
              </tbody>
            </table>
            <div class="clearfix">
              <div class="hint-text">
                Showing <b>5</b> out of <b>25</b> entries
              </div>
              <ul class="pagination">
                <li class="page-item disabled">
                  <a href="#">Previous</a>
                </li>
                <li class="page-item">
                  <a href="#" class="page-link">
                    1
                  </a>
                </li>
                <li class="page-item">
                  <a href="#" class="page-link">
                    2
                  </a>
                </li>
                <li class="page-item active">
                  <a href="#" class="page-link">
                    3
                  </a>
                </li>
                <li class="page-item">
                  <a href="#" class="page-link">
                    4
                  </a>
                </li>
                <li class="page-item">
                  <a href="#" class="page-link">
                    5
                  </a>
                </li>
                <li class="page-item">
                  <a href="#" class="page-link">
                    Next
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
