import '../AdminPage/AdminPage.css'
import React, { useState, useEffect } from "react";
import { useHistory } from 'react-router';

export default function AddAdmin() {
	const [username, setUserName] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [number, setNumber] = useState('');
	const [image, setImage] = useState('');
	const [error, setError] = useState('');
	let history = useHistory();

	// const [postid, setPostId] = useState('');


	const handleSubmit = async (e) => {
		e.preventDefault();

	
		try {
			// const fileInput = document.querySelector('#fileupload') ;
			const formData = new FormData();
		formData.append("UserName", username);
		formData.append("Email", email);
		formData.append("Password", password);
		formData.append("Number", number);
		formData.append('Image', image);

			let url = `//localhost:8000/api/addadmin`
			await fetch(url, {
				method: "post",
				// headers: {
				// 	'Accept': 'application/json',
				// 	'Content-Type': 'multipart/form-data',
				//   },
				body: formData,
			});
		} catch (err) {
			setError( error )
		}
		history.push('/adminpage')
	}

	return (
		// class="modal fade"
		<div id="addEmployeeModal" >
			<div class="modal-dialog">
				<div class="modal-content">
					<form onSubmit={handleSubmit}>
						<div class="modal-header">
							<h4 class="modal-title">Add Admin</h4>
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						</div>
						<div class="modal-body">
							<div class="form-group">


								<label>User Name</label>
								<input type="text"
									class="form-control"
									name="UserName"
									value={username}
									onChange={(e) => setUserName(e.target.value)}
									required />
							</div>


							<div class="form-group">
								<label>Email</label>
								<input type="email"
									class="form-control"
									name="Email"
									value={email}
									onChange={(e) => setEmail(e.target.value)}
									required />
							</div>


							<div class="form-group">
								<label>Password</label>
								<textarea class="form-control"
									name="Password"
									value={password}
									onChange={(e) => setPassword(e.target.value)}
									required></textarea>
							</div>

							<div class="form-group">


								<label>Number</label>
								<input type="text"
									class="form-control"
									name="Number"
									value={number}
									onChange={(e) => setNumber(e.target.value)}
									required />
							</div>

							<div class="form-group">

							<input id="fileupload" type="file" name="Image"
							 onChange={(e) => setImage(e.target.files[0])}
							 required/>
							</div>


							
						</div>
						<div class="modal-footer">
							<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel" />

							<input type="submit" class="btn btn-success" value="Add" />
						</div>
					</form>
				</div>
			</div>
		</div>

	);
}