import './App.css';
import { withRouter, Switch, Route } from "react-router-dom";
import SideNav from './components/SideNav/SideNav';
import AdminPage from './pages/AdminPage/AdminPage'
import AddAdmin from './pages/AddAdmin/AddAdmin'
import EditAdmin from './pages/EditAdmin/EditAdmin'
import HomePage from './pages/HomePage/HomePage'

function App() {
  return (
    <div className="">
      
      {/* <Content/> */}
      

      <SideNav/>
      
      <Switch>
        <Route path="/" exact component={HomePage}/>
        <Route path="/adminpage" component={AdminPage}/>
        <Route path="/addadmin" component={AddAdmin}/>
        <Route path="/editadmin/:id" component={EditAdmin}/>
      </Switch>
      
    </div>
  );
}

export default withRouter(App);